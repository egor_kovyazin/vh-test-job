# Application settings
default["app"]["user"] = "vagrant"
default["app"]["group"] = "vagrant"

default["app"]["server_root"] = "/home/vagrant"
default["app"]["domain"] = "yii-test.local"
default["app"]["logs_root"] = "/home/vagrant/logs"


# Mysql settings
default["mysql"]["root_password"] = ""
