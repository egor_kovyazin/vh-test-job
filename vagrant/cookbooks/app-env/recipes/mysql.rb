# Set root password
bash "init-mysql-root-password" do
  user "root"
  group "root"
  cwd "/tmp"
  code <<-EOH
    echo mysql-server-5.5 mysql-server/root_password password #{node["mysql"]["root_password"]} | debconf-set-selections
    echo mysql-server-5.5 mysql-server/root_password_again password #{node["mysql"]["root_password"]} | debconf-set-selections
  EOH
end

package "mysql-server-5.5" do
  action :install
  options "--force-yes"
end


bash "disable-bind-address" do
  user "root"
  code <<-EOH
    sed -i 's/bind-address\(.*\)=.*/bind-address\1= 0.0.0.0/g' /etc/mysql/my.cnf
    service mysql restart
  EOH
end
