<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_100130_init_db extends Migration {

    public function safeUp() {
        $path = __DIR__ . '/sql_queries/';
        $this->executeSqlFile($path . 'db.sql');
    }

    public function safeDown() {
        $tables = $this->getDbConnection()->createCommand("SHOW TABLES")->queryColumn();
        foreach ($tables as $table)
            if ($table == 'migration') {
                $this->truncateTable($table);
            } else {
                $this->dropTable($table);
            }
    }

    public function executeSqlFile($path) {
        $lines = file($path);
        $count = count($lines);
        $sql = '';
        for ($i = 0; $i < $count; $i++) {
            $lines[$i] = trim($lines[$i]);
            if ($lines[$i] && strpos('#', $lines[$i]) !== 0) {
                $sql .= $lines[$i] . "\n";
                if (substr($lines[$i], -1, 1) === ';') {
                    $this->execute($sql);
                    $sql = '';
                }
            }
        }
    }

}
