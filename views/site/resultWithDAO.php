<?php
/**
 * @var yii\web\View $this
 * @var \yii\data\SqlDataProvider $dataProvider
 */

use yii\grid\GridView;

$this->title = 'task 6';
?>
<div class="site-index">

    <div class="body-content">

        <p class="lead"><strong>Task6:</strong> Реализовать решение 1-ой задачи при помощи DAO.</p>

         <?= GridView::widget([
            'dataProvider' => $dataProvider,
        ]); ?>

    </div>
</div>
