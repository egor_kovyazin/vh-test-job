<?php

/* @var $this yii\web\View */

$this->title = 'Test Yii application';
?>
<div class="site-index">

    <div class="body-content">

        <p class="lead"><strong>Task1:</strong> Каким образом можно получить все записи с привязанным NDC-аттрибутом по запросу для первой таблицы начинающихся на 'title 1'.
Данные должны быть сопряжены с NDC.</p>

        <pre>SELECT s.*, r.NDC FROM tb_source s
LEFT JOIN tb_rel r ON r.cx = s.cx
WHERE title like 'title 1%' AND r.cx IS NOT NULL</pre>

        <br>

        <p class="lead"><strong>Task2:</strong> Дать рекомендации по структуре таблиц и хранению данных.</p>

        <ul>
            <li>Тип атрибутов tb_rel.cx, tb_source.cx, tb_source.rx - varcar(10). Заменить на INT.</li>
            <li>Создать индекс для tb_source.title.</li>
        </ul>

        <br>

        <p class="lead"><strong>Task3:</strong> Подсчитать кол-во записей имеющих больше чем 2 одинаковых вхождений по NDC.</p>

        <pre>SELECT count(amount) FROM (SELECT count(*) AS amount FROM tb_rel GROUP BY ndc HAVING amount > 2) AS subquery</pre>

        <br>

        <p class="lead"><strong>Task4:</strong> Возможно ли упростить структуру. Как бы Вы это решили в контексте 1-ой задачи?
Поясните путь решения и почему сделали так. Условие: Входные данные (таблицы) могут меняться раз в неделю.</p>

        <p>Денормализовать таблицы и добаивть в tb_source атрибут NDC:</p>
        <ul>
            <li>не нужно делать JOIN</li>
            <li>результат запроса можно закешировать</li>
        </ul>

    </div>
</div>
