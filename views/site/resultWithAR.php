<?php
/**
 * @var yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

use yii\grid\GridView;

$this->title = 'task 5';
?>
<div class="site-index">

    <div class="body-content">

        <p class="lead"><strong>Task5:</strong> Реализовать решение 1-ой задачи на базе AR-модели.</p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'cx',
                'rx',
                'title',
                [
                    'attribute' => 'ndc',
                    'value' => function ($model) {
                        return ($model->rels ? $model->rels[0]->ndc: 'none');
                    },
                ],
            ],
        ]); ?>


    </div>
</div>
