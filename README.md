Test project for virtualhealt on Yii 2
============================

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.



RUN UNDER VAGRANT
-------------

### Requirements for developing

    * VirtualBox >= 4.3.10 and Extension Pack
    * Vagrant >= 1.7.2
    * Account on https://atlas.hashicorp.com


### Quick start

    mkdir ~/sites/yii-app && cd ~/sites/yii-app
    
    git clone git@bitbucket.org:egor_kovyazin/vh-test-job.git app && cd app \
    && git submodule sync && git submodule init && git submodule update \
    && cd vagrant \
    && vagrant plugin install vagrant-omnibus && vagrant plugin install vagrant-vbguest && vagrant up
    
    echo -e '\n# Test project\n171.15.34.10 yii-test.local' >> /etc/hosts


When the vagrant environment is up, you'll be able to access http://yii-test.local in your browser
or 171.15.34.10:3306 in your MySQL client (if you don't use Sequel Pro yet, you definitely should).


TASK FOR TEST
-------------

### Задачи:

1. Каким образом можно получить все записи с привязанным NDC-аттрибутом по запросу для первой таблицы начинающихся на 'title 1'. Данные должны быть сопряжены с NDC.
1. Дать рекомендации по структуре таблиц и хранению данных.
1. Подсчитать кол-во записей имеющих больше чем 2 одинаковых вхождений по NDC.
1. Возможно ли упростить структуру. Как бы Вы это решили в контексте 1-ой задачи? Поясните путь решения и почему сделали так. Условие: Входные данные (таблицы) могут меняться раз в неделю.

### На Yii-фреймворке:

1. Реализовать решение 1-ой задачи на базе AR-модели.
1. Реализовать решение 1-ой задачи при помощи DAO.
1. Реализовать интерфейс вывода результатов поставленной задачи с кешированием результата.

Условия:

* Развернуть Yii-приложение через composer.
* Создать все необходимые миграции.