<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Model for table "tb_source"
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * 
 * @property string $cx
 * @property string $rx
 * @property string $title
 */
class Source extends ActiveRecord
{
    public static function tableName()
    {
        return 'tb_source';
    }

    /**
     * Relation for "tb_rel"
     * @return ActiveQuery
     */
    public function getRels()
    {
        return $this->hasMany(Rel::className(), ['cx' => 'cx']);
    }
}
