<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Model for table "tb_rel"
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * 
 * @property string $cx
 * @property string $ndc
 */
class Rel extends ActiveRecord
{
    public static function tableName()
    {
        return 'tb_rel';
    }
}
