<?php

namespace app\controllers\actions;

use app\models\Source;
use yii\base\Action;
use app\components\CachingActiveDataProvider;
use Yii;

/**
 * Action for task1 on ActiveRecord.
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class ResultWithAR extends Action
{
    public function run()
    {
        $dataProvider = new CachingActiveDataProvider([
            'query'         => Source::find()->where("title LIKE 'title 1%'"),
            'pagination'    => ['pageSize' => 20],
            'cacheDuration' => Yii::$app->params['cacheDuration'],
        ]);

        return $this->controller->render('resultWithAR', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
