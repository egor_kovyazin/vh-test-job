<?php

namespace app\controllers\actions;

use yii\base\Action;
use yii\db\Query;
use app\components\CachingSqlDataProvider;
use Yii;

/**
 * Action for task1 on DAO.
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class ResultWithDAO extends Action
{
    public function run()
    {
        $query = new Query();
        $query->select('tb_source.cx, tb_source.rx, tb_source.title, tb_rel.ndc');
        $query->from('tb_source');
        $query->leftJoin('tb_rel', 'tb_rel.cx = tb_source.cx');
        $query->where("tb_source.title like 'title 1%'");

        $dataProvider = new CachingSqlDataProvider([
            'sql'           => $query->createCommand()->sql,
            'totalCount'    => 594042, // TODO: $query->count() very slow -> Nginx 502
            'pagination'    => ['pageSize' => 20],
            'cacheDuration' => Yii::$app->params['cacheDuration'],
        ]);

        return $this->controller->render('resultWithDAO', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
