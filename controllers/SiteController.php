<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'task5' => [
                'class' => 'app\controllers\actions\ResultWithAR',
            ],
            'task6' => [
                'class' => 'app\controllers\actions\ResultWithDAO',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
