<?php

return [
    'class'    => 'yii\db\Connection',
    'dsn'      => 'mysql:host=127.0.0.1;dbname=main_db',
    'username' => 'yii-db-user',
    'password' => 'yii-db-pass',
];
