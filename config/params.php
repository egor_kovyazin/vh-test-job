<?php

return [
    'adminEmail' => 'admin@yii.com',
    'cacheDuration' => 120, // seconds
];
