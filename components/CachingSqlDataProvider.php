<?php

namespace app\components;

use yii\data\SqlDataProvider;

/**
 * SqlDataProvider with cache.
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class CachingSqlDataProvider extends SqlDataProvider
{
    /**
     * @var The number of seconds that query results can remain valid in the cache. If this is
     * not set, the value of [[queryCacheDuration]] will be used instead.
     */
    public $cacheDuration = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (!$this->db) {
            $this->db = \Yii::$app->db;
        }
    }

    /**
     * @inheritdoc
     */
    protected function prepareModels()
    {
        $self = $this;
        $sort = $this->getSort();
        $pagination = $this->getPagination();
        if ($pagination === false && $sort === false) {
            return $this->db->cache(function ($db) use ($self) {
                    return $db->createCommand($self->sql, $self->params)->queryAll();
                }, $this->cacheDuration);
        }

        $sql = $this->sql;
        $orders = [];
        $limit = $offset = null;

        if ($sort !== false) {
            $orders = $sort->getOrders();
            $pattern = '/\s+order\s+by\s+([\w\s,\.]+)$/i';
            if (preg_match($pattern, $sql, $matches)) {
                array_unshift($orders, new Expression($matches[1]));
                $sql = preg_replace($pattern, '', $sql);
            }
        }

        if ($pagination !== false) {
            $pagination->totalCount = $this->getTotalCount();
            $limit = $pagination->getLimit();
            $offset = $pagination->getOffset();
        }

        $sql = $this->db->getQueryBuilder()->buildOrderByAndLimit($sql, $orders, $limit, $offset);

        return $this->db->cache(function ($db) use ($sql, $self) {
                    return $db->createCommand($sql, $self->params)->queryAll();
                }, $this->cacheDuration);
    }
}
