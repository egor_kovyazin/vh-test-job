<?php

namespace app\components;

use yii\db\QueryInterface;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;

/**
 * ActiveDataProvider with cache.
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 */
class CachingActiveDataProvider extends ActiveDataProvider
{
    /**
     * @var The number of seconds that query results can remain valid in the cache. If this is
     * not set, the value of [[queryCacheDuration]] will be used instead.
     */
    public $cacheDuration = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (!$this->db) {
            $this->db = \Yii::$app->db;
        }
    }

    /**
     * @inheritdoc
     */
    protected function prepareModels()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }
        $query = clone $this->query;
        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            $query->limit($pagination->getLimit())->offset($pagination->getOffset());
        }
        if (($sort = $this->getSort()) !== false) {
            $query->addOrderBy($sort->getOrders());
        }
        return $this->db->cache(function ($db) use ($query) {
                return $query->all($db);
            }, $this->cacheDuration);
    }

    /**
     * @inheritdoc
     */
    protected function prepareTotalCount()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }
        $query = clone $this->query;
        $db = $this->db;
        if ($db === null) {
            $modelClass = $this->query->modelClass;
            $db = $modelClass::getDb();
        }
        return $db->cache(function($db) use($query) {
                return (int) $query->limit(-1)->offset(-1)->orderBy([])->count('*', $db);
            }, $this->cacheDuration);
    }

}
